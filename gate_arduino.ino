#include "Gate.h"
#include "Loop.h"

#define loop1Pin 6
#define loop2Pin 7

#define openSwitchPin 8
#define closeSwitchPin 9
#define openStateLedPin 4
#define closeStateLedPin 5

#define openPin 10
#define closePin 11
#define stopPin 12



Loop *loop1 = new Loop(loop1Pin);
Loop *loop2 = new Loop(loop2Pin);
Gate *gate = new Gate(openSwitchPin, closeSwitchPin, openStateLedPin, closeStateLedPin);

void stopCommandAction();
void closeCommandAction();
void openCommandAction();

void setup() {

    pinMode(openPin, INPUT_PULLUP);
    pinMode(closePin, INPUT_PULLUP);
    pinMode(stopPin, INPUT_PULLUP);

    pinMode(13, OUTPUT);
    Serial.begin(9600);

    //gate->close();
}

void loop() {

    int stopPinValue = digitalRead(stopPin);
    int openPinValue = digitalRead(openPin);
    int closePinValue = digitalRead(closePin);

    if(stopPinValue || (openPinValue && closePinValue))
    {
        stopCommandAction();
    }
    else if(openPinValue)
    {
        openCommandAction();
    }
    else if(closePinValue)
    {
        closeCommandAction();
    }
    /*
    Serial.print("stopPinValue : ");
    Serial.println(stopPinValue);
    Serial.print("openPinValue : ");
    Serial.println(openPinValue);
    Serial.print("closePinValue : ");
    Serial.println(closePinValue);
    delay(5000);
     */
}

void stopCommandAction()
{
    digitalWrite(13, HIGH);
    delay(500);
    digitalWrite(13, LOW);
    delay(500);
}
void closeCommandAction()
{
    if((!loop1->hasCar() && !loop2->hasCar()) || gate->isClose())
    {
        gate->close();
    }
    else
    {
        gate->open();
    }
}

void openCommandAction()
{
    gate->open();
}
