//
// Created by oem on 30.10.2020.
//

#include "Gate.h"
#define timeout 200

void Gate::open() {
    if(!this->isClose())
    {
        digitalWrite(closeStateLedPin, LOW);
    }

    if(this->isOpen())
    {
        digitalWrite(openStateLedPin, HIGH);
    }
    else
    {
        digitalWrite(openStateLedPin, HIGH);
        delay(timeout);
        digitalWrite(openStateLedPin, LOW);
        delay(timeout);
    }
}

void Gate::close() {

    if(!this->isOpen())
    {
        digitalWrite(openStateLedPin, LOW);
    }
    if(this->isClose())
    {
        digitalWrite(closeStateLedPin, HIGH);
    }
    else
    {
        digitalWrite(closeStateLedPin, HIGH);
        delay(timeout);
        digitalWrite(closeStateLedPin, LOW);
        delay(timeout);
    }
}

bool Gate::isOpen() {
    return digitalRead(openSwitchPin);
}

bool Gate::isClose() {
    return digitalRead(closeSwitchPin);
}



