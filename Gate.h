//
// Created by oem on 30.10.2020.
//

#ifndef GATEARDUINO_GATE_H

#define GATEARDUINO_GATE_H
#include <Arduino.h>

class Gate {
public:

    Gate(    int openSwitchPin, int closeSwitchPin, int openStateLedPin, int closeStateLedPin)
    {
        this->openStateLedPin = openStateLedPin;
        this->openSwitchPin = openSwitchPin;
        this->closeSwitchPin = closeSwitchPin;
        this->closeStateLedPin = closeStateLedPin;
        pinMode(openStateLedPin , OUTPUT);
        pinMode(closeStateLedPin , OUTPUT);
        pinMode(closeSwitchPin , INPUT_PULLUP);
        pinMode(openSwitchPin , INPUT_PULLUP);

    }

    void open();
    void close();
    //void stopOn();
    //void stopOff();
    bool isOpen();
    bool isClose();


private:

    int openSwitchPin;
    int closeSwitchPin;
    int openStateLedPin;
    int closeStateLedPin;



};


#endif //GATEARDUINO_GATE_H
