set(CMAKE_HOST_SYSTEM "Linux-5.4.0-52-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "5.4.0-52-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/media/oem/tera/CPP/GateArduino/cmake/ArduinoToolchain.cmake")

set(CMAKE_SYSTEM "Arduino")
set(CMAKE_SYSTEM_NAME "Arduino")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
