//
// Created by oem on 30.10.2020.
//

#ifndef GATEARDUINO_LOOP_H
#define GATEARDUINO_LOOP_H
#include <Arduino.h>

class Loop {



public:
    Loop(int pin)
    {
        this->pin = pin;
        pinMode(pin, INPUT_PULLUP);
    }
    bool hasCar();


private:
    int pin;

};


#endif //GATEARDUINO_LOOP_H
